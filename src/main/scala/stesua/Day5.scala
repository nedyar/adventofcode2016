package stesua

import java.security.MessageDigest

/**
  * Created on 07/12/16.
  */
object Day5 extends App {

  val input = "ugkcyxxp"

  val hashInstance = MessageDigest.getInstance("MD5")

  def md5(message: String): String = {
    BigInt(1, hashInstance.digest(message.getBytes)).toString(16)
//    hashInstance.digest(message.getBytes).map("%02X" format _).mkString
  }

  def findNextPasswordChar(id: String, index: Long): (Char, Long) = {
    val message = id + index
    val md = md5(message)
    if (md.startsWith("00000")) {
      val out = (md.charAt(5), index)
      println("First char: " + out)
      return out
    }
    else {
      return findNextPasswordChar(id, index + 1)
    }
  }

  def solve(id: String, solution: String, index: Long): String = solution.length match {
    case 8 => solution.mkString("")
    case _ => {
      val nextPasswordChar = findNextPasswordChar(id, index)
      solve(id, solution + nextPasswordChar._1, nextPasswordChar._2 + 1)
    }
  }

  def solvePerfBoostTry(id: String): String = {
    var solution = ""
    var index = 1
    while (solution.length < 8) {
      val message = id + index
      val md = md5(message)
      if (md.startsWith("00000")) {
        val foundChar = md.charAt(5)
        println("Found char: " + foundChar)
        solution = solution + foundChar
      }
      index = index + 1
    }
    solution
  }

//    val solution = solve("abc", "", 0)
//    println(solution)
//    assert(solution == "18F47A30")
//    println(solve(input, "", 0))

  println(solvePerfBoostTry(input))

}
