package stesua

import scala.annotation.tailrec

/**
  */
object Day13 extends App {

  /**
    * . -> Open space
    * # -> Wall
    */
  type Location = Char

//  val input = 10
    val input = 1364

  type Pos = (Int, Int)

  val startingPosition = (1, 1)

//  val goal = (4, 7)
    val goal = (39, 31)

  def generateLocation(x: Int, y: Int): Location = {
    val z = (x*x + 3*x + 2*x*y + y + y*y) + input
    val binaryZ = z.toBinaryString
    val binaryRapresentation = binaryZ.toCharArray.count(_ == '1')
    if (binaryRapresentation % 2 == 0) '.'
    else '#'
  }

  lazy val office = Stream.tabulate(Integer.MAX_VALUE,Integer.MAX_VALUE) ((x, y) => generateLocation(y, x))

  def getNeighbours(positions: Set[Pos]): Set[Pos] = {
    val offsets = List((-1, 0), (1, 0), (0, -1), (0, 1))

    for {
      pos <- positions
      (r,c) <- offsets.toSet
      (r1, c1) = (pos._1 + r, pos._2 + c)
      if r1 >= 0 && c1 >= 0
      if office(r1)(c1) == '.'
    } yield (r1, c1)
  }

  @tailrec
  def solve(visited: Set[Pos], actualPositions: Set[Pos], goal: Pos, counter: Int): Int = {
    if (actualPositions.contains(goal)) counter
    else {
      val neighbours = getNeighbours(actualPositions)
      solve(visited.union(actualPositions), neighbours.diff(visited.union(actualPositions)), goal, counter + 1)
    }
  }

//  println(office.take(100).map(_.take(100).mkString(" ")).mkString("\n"))

  println(s"Solution part 1: ${solve(Set(), Set(startingPosition), goal, 0)}")

  @tailrec
  def solvePart2(visited: Set[Pos], actualPositions: Set[Pos], goal: Pos, counter: Int): Int = {
    if (counter > 50) visited.size
    else {
      val neighbours = getNeighbours(actualPositions)
      solvePart2(visited.union(actualPositions), neighbours.diff(visited.union(actualPositions)), goal, counter + 1)
    }
  }

  println(s"Solution part 2: ${solvePart2(Set(), Set(startingPosition), goal, 0)}")

}
