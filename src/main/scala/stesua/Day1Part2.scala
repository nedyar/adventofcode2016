package stesua

object Day1Part2 extends App {

  case class Position(x: Int, y: Int, facing: Char)

  val input = "L2,L5,L5,R5,L2,L4,R1,R1,L4,R2,R1,L1,L4,R1,L4,L4,R5,R3,R1,L1,R1,L5,L1,R5,L4,R2,L5,L3,L3,R3,L3,R4,R4,L2,L5,R1,R2,L2,L1,R3,R4,L193,R3,L5,R45,L1,R4,R79,L5,L5,R5,R1,L4,R3,R3,L4,R185,L5,L3,L1,R5,L2,R1,R3,R2,L3,L4,L2,R2,L3,L2,L2,L3,L5,R3,R4,L5,R1,R2,L2,R4,R3,L4,L3,L1,R3,R2,R1,R1,L3,R4,L5,R2,R1,R3,L3,L2,L2,R2,R1,R2,R3,L3,L3,R4,L4,R4,R4,R4,L3,L1,L2,R5,R2,R2,R2,L4,L3,L4,R4,L5,L4,R2,L4,L4,R4,R1,R5,L2,L4,L5,L3,L2,L4,L4,R3,L3,L4,R1,L2,R3,L2,R1,R2,R5,L4,L2,L1,L3,R2,R3,L2,L1,L5,L2,L1,R4"

  val positions = input.split(",")
    .toSeq
    .map(i => i.head -> i.tail.toInt)
    .flatMap { case (c, i) => (c, 1) +: Seq.fill(i - 1)(('N', 1)) }
    .scanLeft(Position(0, 0, 'N')) { (pos, tup) =>
      tup match {
        case ('L', n) if pos.facing == 'N' => Position(pos.x - n, pos.y, 'W')
        case ('L', n) if pos.facing == 'W' => Position(pos.x, pos.y - n, 'S')
        case ('L', n) if pos.facing == 'E' => Position(pos.x, pos.y + n, 'N')
        case ('L', n) if pos.facing == 'S' => Position(pos.x + n, pos.y, 'E')

        case ('R', n) if pos.facing == 'N' => Position(pos.x + n, pos.y, 'E')
        case ('R', n) if pos.facing == 'W' => Position(pos.x, pos.y + n, 'N')
        case ('R', n) if pos.facing == 'E' => Position(pos.x, pos.y - n, 'S')
        case ('R', n) if pos.facing == 'S' => Position(pos.x - n, pos.y, 'W')

        case ('N', n) if pos.facing == 'N' => Position(pos.x, pos.y + n, 'N')
        case ('N', n) if pos.facing == 'W' => Position(pos.x - n, pos.y, 'W')
        case ('N', n) if pos.facing == 'E' => Position(pos.x + n, pos.y, 'E')
        case ('N', n) if pos.facing == 'S' => Position(pos.x, pos.y - n, 'S')
      }
    }
    .map(position => position.x -> position.y  )

  println(positions)
  val dedup = positions.diff(positions.distinct)
  val dest = dedup.head
  println(Math.abs(dest._1) + Math.abs(dest._2))
}
