package stesua

import scala.io.Source

/**
  * Created on 07/12/16.
  */
object Day4 {

  val input = Source.fromResource("day4input.txt").getLines.toList

  type Triangle = (Int, Int, Int)

  case class Room(words: String, checksum: String, id: Int) {
    def isRealRoom(): Boolean = {
      this.checksum == calculateChecksum()
    }

    def calculateChecksum(): String = {
      val wordCount = this.words.filter(_.isLetter)
        .toList
        .groupBy((char: Char) => char)
        .mapValues(_.length)
        .toList
        .sortWith{ case ((n1, m1), (n2, m2))  =>
          if ( m1 > m2 ) true
          else if ( m1 == m2 && n1 < n2 ) true
          else false
        }

      wordCount.take(5)
        .map(_._1)
        .mkString("")
    }
  }

  def lineToRoom(line: String): Room = {
    val pattern = """(.*)-(\d+)\[(.*)\]""".r
    val pattern(words, id, checksum) = line
    Room(words, checksum, id.toInt)
  }

  def solve(sum: Int, rooms: List[String]): Int = rooms match  {
    case Nil => sum
    case x :: xs => {
      val room = lineToRoom(x)
      solve(if (room.isRealRoom()) sum + room.id else sum, xs)
    }
  }

  def main(args: Array[String]): Unit = {
    val realRooms = solve(0, input)
    println(realRooms)
  }
}
