package stesua

import scala.io.Source

/**
  */
object Day9 extends App {

  val input = Source.fromResource("day9input.txt").mkString
  val inputTest = List(
    ("ADVENT", "ADVENT", 6),
    ("A(1x5)BC", "ABBBBBC", 7),
    ("(3x3)XYZ", "XYZXYZXYZ", 9),
    ("A(2x2)BCD(2x2)EFG", "ABCBCDEFEFG", 11),
    ("(6x1)(1x3)A", "(1x3)A", 6),
    ("X(8x2)(3x3)ABCY", "X(3x3)ABC(3x3)ABCY", 18)
  )

  val markerPatternStr = """(\w*)\((\d+)x(\d+)\)(.*)"""
  val markerPattern = markerPatternStr.r

  def solve(in: String, out: String): String = in match {
    case s if (s.isEmpty) => {
      out
    }
    case markerPattern(prefix, numChars, times, postfix) => {
      val producedString = postfix.take(numChars.toInt) * times.toInt
      solve(postfix.drop(numChars.toInt), out + prefix + producedString)
    }
    case s => solve("", out + s)
  }

  inputTest.zipWithIndex.foreach{ tuple =>
    println("Test " + tuple._2)
    val solution = solve(tuple._1._1, "")
    println(s"Solution string: ${solution} solution number ${solution.length}")
    println(s"Correct?: ${solution == tuple._1._2}")
    println("#" * 20)
  }

  val solution = solve(input, "")
  println(s"Solution: ${solution.length}, string: ${solution}")


  val markerPattern2Str = """(\w*)(\([\d+]x[\d+]\))*(.*)"""
  val markerPattern2 = markerPattern2Str.r
  val tokenPatter = """"\"""

//  def solvePart2(in: String, out: String): String = in match {
//    case s if (s.isEmpty) => {
//      out
//    }
//    case markerPattern(prefix, token, postfix) => {
//      val
//      val producedString = postfix.take(numChars.toInt) * times.toInt
//      solvePart2(postfix.drop(numChars.toInt), out + prefix + producedString)
//    }
//    case s => solvePart2("", out + s)
//  }
}
