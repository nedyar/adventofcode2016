package stesua

object Day1 extends App {

  val input =
    """L2, L5, L5, R5, L2, L4, R1, R1, L4, R2, R1, L1, L4, R1, L4, L4, R5, R3, R1, L1, R1, L5, L1, R5, L4,
      |R2, L5, L3, L3, R3, L3, R4, R4, L2, L5, R1, R2, L2, L1, R3, R4, L193, R3, L5, R45, L1, R4, R79, L5, L5, R5,
      |R1, L4, R3, R3, L4, R185, L5, L3, L1, R5, L2, R1, R3, R2, L3, L4, L2, R2, L3, L2, L2, L3, L5, R3, R4, L5, R1,
      |R2, L2, R4, R3, L4, L3, L1, R3, R2, R1, R1, L3, R4, L5, R2, R1, R3, L3, L2, L2, R2, R1, R2, R3, L3, L3,
      |R4, L4, R4, R4, R4, L3, L1, L2, R5, R2, R2, R2, L4, L3, L4, R4, L5, L4, R2, L4, L4, R4, R1, R5, L2, L4,
      |L5, L3, L2, L4, L4, R3, L3, L4, R1, L2, R3, L2, R1, R2, R5, L4, L2, L1, L3, R2, R3, L2, L1, L5, L2,
      |L1, R4""".stripMargin

  def rotate(direction: Direction.Value, vector: (Int, Int)): (Int, Int) = {
    val cosTheta = 0
    val sinTheta = if (direction == Direction.Right) -1 else 1
    (vector._1 * cosTheta - vector._2 * sinTheta, vector._1 * sinTheta + vector._2 * cosTheta)
  }

  def move(action: Action, previoustState: State): State = {
    val newVector = rotate(action.direction, previoustState.vector)
    val newPosition = (action.n * newVector._1 + previoustState.position._1,
      action.n * newVector._2 + previoustState.position._2 )

    println(s"Action $action")
    println(s"Vector from ${previoustState.vector} to $newVector")
    println(s"Position from ${previoustState.position} to $newPosition")
    State(newPosition, newVector)
  }

  def positionExplored(from: (Int, Int), to : (Int, Int)): List[(Int, Int)] =  {
    val iRange = (math.min(from._1, to._1) to math.max(from._1, to._1))
    val jRange = (math.min(from._2, to._2) to math.max(from._2, to._2))
    val out = for {
      i <- iRange
      j <- jRange
    } yield (i, j)
    out.toList.drop(1)
  }

  def explore(actions: List[Action], state: State, reversedPositionHistory: List[(Int, Int)]): (State, List[(Int, Int)]) = actions match {
    case Nil => (state, reversedPositionHistory)
    case x :: xs => {
      val nextState: State = move(x, state)
      val visitedPosition: List[(Int, Int)] = positionExplored(state.position, nextState.position)
      explore(xs, nextState, visitedPosition ++ reversedPositionHistory)
    }
  }

  def firstTwiceVisitedLocation(reversedStateHistory: List[(Int, Int)]) : (Int, Int) =  {
    val groupedByPosition = reversedStateHistory
        .reverse
        .zipWithIndex
        .groupBy(_._1)
        .map{case (state, tuples) => (state, tuples.map(_._2))}

        val visitedTwice = groupedByPosition.filter(_._2.length > 1)

        val sortedBySecondVisit = visitedTwice.map{case (state, list) => (state, list(1))}.toSeq.sortWith(_._2 < _._2)

        sortedBySecondVisit(0)._1

  }

  def distance(from: State, to: State): Int = {
    distance(from.position, to.position)
  }

  def distance(fromPosition: (Int, Int), toPosition: (Int, Int)): Int = {
    axisDistance(fromPosition._1, toPosition._1) + axisDistance(fromPosition._2, toPosition._2)
  }

  def axisDistance(from: Int, to: Int): Int = {
    math.abs(math.max(from, to) - math.min(from, to))
  }

  val startPosition = (0, 0)
  val startingVector = (0, 1)
  val initalState = State(startPosition, startingVector)
  val actions = input.split(",").map(_.trim).map(Action.fromString(_).get)

  val solution: (State, List[(Int, Int)]) = explore(actions.toList, initalState, List())
  val finalState = solution._1
  println(s"Distance from $initalState to $finalState: ${distance(initalState, finalState)}");

  val reversedStateHistory = solution._2
  val firstTwiceVisitedState = firstTwiceVisitedLocation(reversedStateHistory)
  println(s"First twice visited: ${firstTwiceVisitedState}")
  println(s"Distance from first twice visited: ${distance(startPosition, firstTwiceVisitedState)}")

}

case class State(position: (Int, Int), vector: (Int, Int))

object Direction extends Enumeration {
  val Left, Right = Value
}
case class Direction(direction: Direction)

object Action {
  def fromString(str: String): Option[Action] = {
    if (str.startsWith("L")) {
      return Some(new Action(Direction.Left, str.replaceAll("L","").toInt))
    }
    else if (str.startsWith("R")) {
      return  Some(new Action(Direction.Right, str.replaceAll("R","").toInt))
    }
    None
  }
}

class Action(val direction: Direction.Value, val n: Int) {
  override def toString: String = s"<$direction, $n>"
}




