package stesua

import scala.io.Source

/**
  * Created on 07/12/16.
  */
object Day8 {

  val input = Source.fromResource("day8input.txt").getLines.toList
  val inputTest = Source.fromResource("day8inputTest.txt").getLines.toList

  case class Display(val grid: Array[Array[Boolean]]) {

    def rect(col: Int, row: Int) = {
      for {
        r <- 0 to row - 1
        c <- 0 to col - 1
      } grid(r)(c) = true
    }

    /**
      * Rotate row
      *
      * @param row
      * @param by
      * @return
      */
    def rotateR(row: Int, by: Int): Unit = {
      val extractedRow = grid(row)
      grid(row) = rotateArray(extractedRow, by)
    }

    /**
      * Rotate column
      *
      * @param col
      * @param by
      * @return
      */
    def rotateC(col: Int, by: Int) = {
      val arrayToRotate = for {
        r <- 0 to grid.size - 1
      } yield grid(r)(col)

      val rotatedArray = rotateArray(arrayToRotate.toArray, by)
      for {
        r <- 0 to grid.size - 1
      } grid(r)(col) = rotatedArray(r)
    }

    def rotateArray(array: Array[Boolean], by: Int): Array[Boolean] = {
      val size = array.size
      array.drop(size - (by % size)) ++ array.take(size - (by % size))
    }

    def print(): Unit = {
      println("#" * (grid.head.size * 2 - 1))
      val matrix: Array[Array[String]] = grid.map(_.map(if (_) "\u25A0" else " "))
      println(matrix.map(_.mkString("|")).mkString("\n"))
      println("#" * (grid.head.size * 2 - 1))
    }

    def count(status: Boolean): Int = {
      val ones = for {
        r <- 0 to grid.size - 1
        c <- 0 to grid(r).size - 1
        if grid(r)(c) == status
      } yield 1
      ones.sum
    }
  }

  def manual(): Unit = {
    val grid = Array.fill(3, 7)(false)
    val display = Display(grid)
    display.print()
    display.rect(3, 2)
    display.print()
    display.rotateC(1, 1)
    display.print()
    display.rotateR(0, 4)
    display.print()
    display.rotateC(1, 1)
    display.print()
  }

  def test(): Unit = {
    val grid = Array.fill(3, 7)(false)
    val display = Display(grid)
    solve(display, inputTest)
    display.print()
    println("Solution: " + display.count(true))
  }

  def main(args: Array[String]): Unit = {
    manual()
    test()

    val grid = Array.fill(6, 50)(false)
    val display = Display(grid)
    solve(display, input)
    display.print()
    println("Solution: " + display.count(true))
  }

  def solve(display: Display, in: List[String]): Unit = {
    in.foreach { line =>
      val rectRegexStr = "rect (\\d+)x(\\d+)"
      val rotateRowRegexStr = "rotate row y=(\\d+) by (\\d+)"
      val rotateColumnRegexStr = "rotate column x=(\\d+) by (\\d+)"

      if (line.matches(rectRegexStr)) {
        val pattern = rectRegexStr.r
        val pattern(col, row) = line
        display.rect(col.toInt, row.toInt)
      }
      else if (line.matches(rotateColumnRegexStr)) {
        val pattern = rotateColumnRegexStr.r
        val pattern(col, by) = line
        display.rotateC(col.toInt, by.toInt)
      }
      else if (line.matches(rotateRowRegexStr)) {
        val pattern = rotateRowRegexStr.r
        val pattern(row, by) = line
        display.rotateR(row.toInt, by.toInt)
      }
      display.print()
    }
  }
}

