package stesua.common

import java.security.MessageDigest

import org.apache.commons.codec.binary.Hex

import scala.annotation.tailrec

/**
  * Created by Stefano on 19/12/2016.
  */
object HashUtil {

  private val hashInstance = MessageDigest.getInstance("MD5")

  def md5(message: String): String = {
    val bytes = hashInstance.digest(message.getBytes)
    // performe a lot better then .map("%02X" format _).mkString
    Hex.encodeHexString(bytes)
  }

  //  def md5(message: String): String = {
  //    BigInt(1, hashInstance.digest(message.getBytes)).toString(16)
  //    //    hashInstance.digest(message.getBytes).map("%02X" format _).mkString
  //  }


  private var md5Cache = scala.collection.mutable.HashMap[String, String]()
  def md5Cached(message: String): String = {
    md5Cache.getOrElseUpdate(message, md5(message))
  }

  def stretchedMd5(message: String): String = {
    @tailrec
    def stretchedMd5(message: String, index: Int): String = {
      if (index == 2017) message
      else {
        val nextMD = HashUtil.md5(message).toLowerCase()
        stretchedMd5(nextMD, index + 1)
      }
    }
    stretchedMd5(message, 0)
  }

  private var stretchedMd5Cache = scala.collection.mutable.HashMap[String, String]()
  def stretchedMd5Cached(message: String): String = {
      stretchedMd5Cache.getOrElseUpdate(message, stretchedMd5(message))
  }
}
