package stesua

import stesua.common.HashUtil

import scala.collection.mutable

/**
  */
object Day14 extends App {

  def digitsInARow(n: Int, s: String): Option[String] = {
    val nDigitsRE = s"(\\w)\\1{${n-1}}".r
    nDigitsRE.findFirstIn(s)
  }

  def containsFiveDigitInARow(s: String, toFound: String): Boolean = {
    val re = s"(${toFound.head})\\1{4}".r
    val out = re.pattern.matcher(s).find()
    out
  }

  def fiveInARow(foundTriplet: String, index: Int, messageStream: Stream[String]): Boolean = {
    val sliced = messageStream
      .slice(index + 1, index + 1000)
    val indexWhere = sliced.indexWhere(containsFiveDigitInARow(_, foundTriplet))
//    if (indexWhere > -1) println(s"Sol: $index -> ${indexWhere + index}")
    sliced.exists(containsFiveDigitInARow(_, foundTriplet))
  }


  def solve(messageStream: Stream[String]): Stream[Int] = {
    for {
      (md, index) <- messageStream.zipWithIndex
      foundDigit <- digitsInARow(3, md)
      if fiveInARow(foundDigit, index, messageStream)
    } yield index + 1
  }


  def solve(input: String, hashFunction: String => String): Int = {
    var buffer: mutable.SortedMap[Int, String] = mutable.TreeMap[Int, String]()
    var key = 0
    var index = 1
    var solList = List[(Int, Int)]()
    while (key <= 64) {

      val md = hashFunction(input + index.toString)
      buffer.retain((k, v) => index - k <= 1000)

      for ((k, v) <- buffer; if containsFiveDigitInARow(md, v)) {
        key = key + 1
        solList = (k, index) :: solList
//        println(s"partial sol: $k, $index")
      }

      digitsInARow(3, md) match {
        case Some(fnd) => buffer.put(index, fnd)
        case None => // NOP
      }

      index = index + 1
    }
    val sorted = solList.sortBy(_._1)
    sorted(63)._1 // two line to avoid: solList.sortBy(_._1)(Ordering[Int])(63)._1
  }
  val inputTest = "abc"
  val input = "yjdafjpo"

  println(s"Soluzione parte 1 (test) : ${solve(inputTest, HashUtil.md5)}")
  println(s"Soluzione parte 1 : ${solve(input, HashUtil.md5)}")

  println(s"Soluzione parte 2 (test) : ${solve(inputTest, HashUtil.stretchedMd5)}")
  println(s"Soluzione parte 2 : ${solve(input, HashUtil.stretchedMd5)}")

  lazy val messageDigests = Stream.from(1).map(int => HashUtil.md5(input + int.toString))
  lazy val messageDigestsStretched = Stream.from(1).map(int => HashUtil.stretchedMd5Cached(input + int.toString))

  println(s"Soluzione parte 1: ${solve(messageDigests).take(64).toList(63)}")
  println(s"Soluzione parte 2: ${solve(messageDigestsStretched).take(64).toList(63)}")

}
