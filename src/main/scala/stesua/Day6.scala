package stesua

import scala.io.Source

/**
  * Created on 07/12/16.
  */
object Day6 extends App {

  val input = Source.fromResource("day6input.txt").getLines.toList

  val transposed = input.map(_.toList).transpose

  val groupedByCount = transposed
    .map(
      _.groupBy((char: Char) => char)
        .mapValues(_.length)
        .toList
        .sortWith(_._2 > _._2)
    )

  println(groupedByCount.map(_.take(1).head._1).mkString(""))
}
