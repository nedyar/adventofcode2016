package stesua


import scala.io.Source

/**
  * Created on 07/12/16.
  */
object Day7 extends App {

  val input = Source.fromResource("day7input.txt").getLines.toList
  val inputTest = Source.fromResource("day7inputTest.txt").getLines.toList

  case class IP7(addresses: List[String], hypernetAddresses: List[String]) {

    private def containsAbba(s: String): Boolean = {

      // TODO use sliding(4) instead of substring
      for( i <- 0 to s.size - 4){
        val substring: String = s.substring(i, i + 4)
        if (isAbba(substring)) {
          println("Contained abba: " + substring)
          return true
        }
      }
      false
    }

    private def isAbba(s: String): Boolean = {
      s.length == 4 && s.reverse == s && s(0) != s(1)
    }

    def supportTLS(): Boolean = {
      !hypernetAddresses.exists(containsAbba(_)) && addresses.exists(containsAbba(_))
    }
  }

  def extractInfo(line: String) : IP7 = {
    val modLine = line.replaceAll("\\[", "_").replaceAll("\\]","_")
    val splitted = modLine.split("_")

    //   odd are addresses, even are hypernet
    val partitioned = splitted.zipWithIndex.partition(pair => (pair._2 % 2 == 0))

    IP7(partitioned._1.map(_._1).toList, partitioned._2.map(_._1).toList)
  }

  def solve(counter: Int, input: List[String]): Int = input match {
    case Nil => counter
    case x :: xs => {
      val ip7 = extractInfo(x)
      val supportTLS = ip7.supportTLS()
      if (supportTLS) {
        println("Solution " + x)
      }
      solve(if (supportTLS) counter + 1 else counter, xs)
    }
  }

// val solution = solve(0, inputTest)
  val solution = solve(0, input)
  println(solution)
}
