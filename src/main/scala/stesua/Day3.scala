package stesua

import scala.io.Source

/**
  * Created on 07/12/16.
  */
object Day3 extends App {

  val input = Source.fromResource("day3input.txt").getLines.toList

  type Triangle = (Int, Int, Int)

  def isFeasible(triangle: Triangle): Boolean = triangle match {
    case (x, y, z) if (x + y <= z) => false
    case (x, y, z) if (x + z <= y) => false
    case (x, y, z) if (y + z <= x) => false
    case (_, _, _) => true
  }

  def lineToTriangle(line: String): Triangle = {
    listToTriangle(line.split(" ").filterNot(_.isEmpty).map(_.toInt).toList)
  }

  def listToTriangle(list: List[Int]): Triangle = list match {
    case List(x, y, z) => (x, y, z)
  }

  val feasiblesTrianlges = input.map(lineToTriangle(_)).count(isFeasible(_))
  println(feasiblesTrianlges)

}
